@extends('master_full')

@section('page_title')
{{ __('lg.connect.login') }}
@endsection

@section('additionals_js_files')
    <script type="text/javascript" src="{{ url('/static/js/connect.js?v='.time()) }}"></script>
@endsection

@section('content')
<div class="page_app">
    <div class="box box_app">
        <div class="logo logo_app">
            <img src="{{ url('/static/images/logo.png') }}" alt="{{ config('cms.app_name') }}">
        </div>
        <h2 class="title">
            {{ __('lg.connect.login') }}
        </h2>

        <div class="form mtop32">
            {!! Form::open(['url'=> '/', 'id' => 'form_connect_login', 'autocomplete' => 'off']) !!}
            {!! Form::text('autocomplete', null ,['class' => 'autocomplete']) !!}
                <label for="email" class="mtop16">{{ __('lg.connect.email') }}:</label>
                <div class="group mtop8">
                    <i class="bi bi-envelope-open"></i>
                    {!! Form::email('email', null, ['class' => 'input disableac']) !!}
                </div>

                <label for="password" class="mtop16">{{ __('lg.connect.password') }}:</label>
                <div class="group mtop8">
                    <i class="bi bi-fingerprint"></i>
                    {!! Form::password('password', ['class' => 'input disableac', 'id' => 'input_password']) !!}
                </div>
                <a href="#" class="show_password mtop16 " data-state="hide" data-target="input_password" id="show_password_login">{{ __('lg.connect.show_password') }}</a>

                {!! Form::submit( __('lg.connect.connect'), ['class' => 'btn mtop45 '] )!!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

