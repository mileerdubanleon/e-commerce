<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@section('page_title')@show - Mileer APP</title>
    <link rel="icon" href="{{ url('/favicon.ico') }}" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="routeName" content="{{ Route::currentRouteName() }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spline+Sans:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/static/css/mdalert.css') }}">
    <link rel="stylesheet" href="{{ url('/static/css/connect.css') }}">

    <script type="text/javascript" src="{{ url('/static/js/lang/'.config('mileer.languaje').'.js?v='.time()) }}"></script>
    <script type="text/javascript" src="{{ url('/static/js/app.js?v='.time()) }}"></script>
    @section('additionals_js_files')
    @show
    
</head>
<body>
    @include('components.loader_action')
    @include('components.mdalert')
    <div class="wrapper">
        @section('content')
        @show
    </div>
    

    <script type="text/javascript" src="{{ url('/static/js/mdalert.js') }}"></script>
</body>
</html>