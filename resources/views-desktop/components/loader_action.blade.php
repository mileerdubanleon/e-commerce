<div class="loader_action" id="loader_action">
    <div class="action">
        <div class="col">
            <img src="{{ url('/static/images/loading.gif') }}" style="width: 40px;" alt="{{ __('lg.general.loader_action_text') }}">
        </div>
        <div class="col">{{ __('lg.general.loader_action_text') }}</div>
    </div>
</div>