<?php 

    return [
        'general' => [
            'loader_action_text' => 'Estamos haciendo algo, un momento.',
            'close_md_alert' => 'Cerrar',
            'error' => 'Ha ocurrido un error',
            'error_validation' => 'Fallo de validación'
        ],
        'connect' => [
            'login' => 'Ingresar a mi cuenta',
            'email' => 'Correo eletrónico',
            'password' => 'Contraseña',
            'connect' => 'Ingresar',
            'v_email_required' => 'El correo electrónico es requerido',
            'v_email_email' => 'El formato de correo electrónico es inválido',
            'v_password_required' => 'La contraseña es requerida',
            'v_password_min' => 'La contraseña debe tener al menos 8 caracteres',
            'connect_fail' => 'Correo electrónico o contraseña errónea',
            'show_password' => 'Mostrar contraseña',

        ]
    ];

?>